{ pkgs, ... }:
{
  users.users.matteo = {
    isNormalUser = true;
    home = "/home/matteo";
    description = "Matteo";
    extraGroups = [ "wheel" "networkmanager" "libvirtd" ];
    hashedPassword = "$y$j9T$ot0G7/sENkVxiul7iBqqu1$TYo1t26iwDIPy0C/BrtTAZfPBtjMRg1uA1vHPJNZrL2";
    packages = with pkgs; [
       
    ];
  }; 
}
