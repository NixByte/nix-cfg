{
  imports = [
   ./boot
   ./locale
   ./nw
   ./pkgs
   ./services
   ./users
   ./sound
   ./hardware
  ];
}
