{ pkgs, ... }:
{
  environment.systemPackages = with pkgs; [
    psmisc
    tree
    curl
    wget
    unzip
    p7zip
  ];

  imports = [
    ./vim
    ./fzf
    ./htop
    ./locate
    ./steam
    ./podman
  ];
}
