{
  time.timeZone = "Europe/Rome";
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    earlySetup = true;
    font = "latarcyrheb-sun32";
    keyMap = "us";
  };
}
