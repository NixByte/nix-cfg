{ pkgs, ... }:
{
  programs.kitty = {
    enable = true;
    theme = "Doom Vibrant";
    font.name = "null";
    font.package = pkgs.fira-code;
    font.size = 21;
  };
}
