{ pkgs, ... }:
{
  programs.eww = {
    enable = true;
    configDir = ./bar;
  };

  home.packages = with pkgs; [
    pamixer
    alsa-utils
  ];
}
