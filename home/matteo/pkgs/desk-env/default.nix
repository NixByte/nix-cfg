{ pkgs, pkgs-unstable, ... }:
{
  imports = [
    ./hyprland
    ./tofi
    ./eww
    ./ags
  ];

  home.packages = with pkgs-unstable; [
    xwayland
    xdg-desktop-portal-hyprland
  ];
}
