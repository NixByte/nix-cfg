{ pkgs, pkgs-unstable, ... }:
{
  wayland.windowManager = {
    hyprland = {
      enable = true;
      package = pkgs-unstable.hyprland;
      systemd.enable = false;
    };
  };
}
