{ pkgs, ... }:
{
  home.packages = with pkgs; [
    vesktop
    prismlauncher
    gamescope
    lutris
  ];
  
  imports = [
    ./desk-env
    ./kitty
    ./firefox
    ./git
  ];
}
