{
  imports = [
    ./dotfiles
    ./pkgs
    ./services
  ];
  home.username = "matteo";
  home.homeDirectory = "/home/matteo";

  home.stateVersion = "24.05";
  programs.home-manager.enable = true;
}
