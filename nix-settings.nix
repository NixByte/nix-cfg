{ pkgs, ... }:
{
  nix = {
    package = pkgs.nixFlakes;
    extraOptions = 
    ''
      experimental-features = nix-command flakes
    '';
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 15d";
    };
  };
}
